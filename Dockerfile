FROM java:8-jre

COPY ["target/*.jar", "/tmp"]

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "tmp/demo-0.0.1-SNAPSHOT.jar"]

